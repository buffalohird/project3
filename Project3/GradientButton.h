//
//  GradientButton.h
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface GradientButton : UIButton {
    CAGradientLayer *shineLayer;
    CALayer *highlightLayer;
}
- (id)initWithLayers;
- (void)createBorder;
- (void)createShine;
- (void)createHighlightLayer;
- (void)createLayers;

@end
