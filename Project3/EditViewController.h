//
//  EditViewController.h
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EffectsFilter.h"
#import "UIImage+Crop.h"
#import <QuartzCore/QuartzCore.h>

@interface EditViewController : UIViewController <UIGestureRecognizerDelegate> {
    CGPoint _translation;
}


@property (nonatomic, retain) UIImage *photo;
@property (nonatomic) int effect;
@property (nonatomic, retain) NSString *effectName;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (nonatomic, retain) UIImageView *photoView;
@property (nonatomic, retain) UIImage *originalPhoto;
@property (nonatomic, strong) EffectsFilter *effectsFilter;
@property (assign, nonatomic, readwrite) float scale;


- (IBAction)done:(id)sender;
- (IBAction)resetPhoto:(id)sender;
- (IBAction)savePhoto:(id)sender;
- (IBAction)cropPhoto:(id)sender;
- (void)commitCropPhoto:(CGRect)cropRect;
- (void)panPhoto:(UIPanGestureRecognizer *)gestureRecognizer;
- (void)scalePhoto:(UIPinchGestureRecognizer *)gestureRecognizer;
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer;
@end
