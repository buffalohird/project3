//
//  SecondViewController.m
//  Project3
//
//  Created by Buffalo Hird on 4/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SecondViewController.h"


@interface SecondViewController ()

@end

@implementation SecondViewController

@synthesize photoView = _photoView;
@synthesize chooseButton = _chooseButton;
@synthesize takeButton = _takeButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Upload", @"Second");
        self.tabBarItem.image = [UIImage imageNamed:@"second"];
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.takeButton = [[GradientButton alloc] init];
    self.takeButton.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    [self.takeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.takeButton addTarget:self 
               action:@selector(getPhoto:)
     forControlEvents:UIControlEventTouchUpInside];
    [self.takeButton setTitle:@"Take Photo" forState:UIControlStateNormal];
    self.takeButton.frame = CGRectMake(175.0, 354.0, 125.0, 40.0);
    [self.takeButton finalize];
    [self.view addSubview:self.takeButton];
    
    self.chooseButton = [[GradientButton alloc] init];
    self.chooseButton.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    [self.chooseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.chooseButton addTarget:self 
               action:@selector(getPhoto:)
     forControlEvents:UIControlEventTouchUpInside];
    [self.chooseButton setTitle:@"Choose photo" forState:UIControlStateNormal];
    self.chooseButton.frame = CGRectMake(20.0, 354.0, 125.0, 40.0);
    [self.chooseButton finalize];
    [self.view addSubview:self.chooseButton];
    
    self.photoView = [[UIImageView alloc] init];
    self.photoView.frame = CGRectMake(20.0, 20.0, 280.0, 314.0);
    self.photoView.backgroundColor = [UIColor blackColor];
    //self.photoView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.photoView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


// set up called when an image has been selected
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
	[picker dismissModalViewControllerAnimated:YES];
	self.photoView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    self.photoView.contentMode = UIViewContentModeScaleAspectFill;
    self.photoView.backgroundColor = [UIColor lightGrayColor];
    
    self.chooseButton = [[GradientButton alloc] init];
    self.chooseButton.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    [self.chooseButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.chooseButton addTarget:self 
                          action:@selector(sendPhoto:)
                forControlEvents:UIControlEventTouchUpInside];
    [self.chooseButton setTitle:@"Choose photo" forState:UIControlStateNormal];
    self.chooseButton.frame = CGRectMake(20.0, 10.0, 125.0, 40.0);
    [self.chooseButton finalize];
    [self.view addSubview:self.chooseButton];
}

- (void)getPhoto:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
    
    
    // if a photo is to be chosen, display the user's photos
	if((UIButton *) sender == self.chooseButton) 
    {
		picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    // if a photo is to be taken, display the user's camera
	} else if((UIButton *) sender == self.takeButton)
    {
		picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	}
    
	[self presentModalViewController:picker animated:YES];
}

- (void)sendPhoto:(id)sender
{

    // send the selected photo to the effectsViewController, so that an effect can be chosen
    EffectsViewController *effectsViewController = [[EffectsViewController alloc] init];
    effectsViewController.photo = self.photoView.image; 
    [self presentModalViewController:effectsViewController animated:YES];
}




@end
