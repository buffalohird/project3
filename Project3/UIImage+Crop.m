//
//  UIImage+Crop.m
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImage+Crop.h"

@implementation UIImage (Crop)

- (UIImage *)crop:(CGRect)rect scalable:(BOOL)scaleBool
{
    
    CGFloat scale;
    if(scaleBool == YES)
    {
        scale = [[UIScreen mainScreen] scale];
    }
    
    else
    {
        scale = 1;
    }
    
    if (scale > 1.0)
    {
        rect = CGRectMake(rect.origin.x*scale, rect.origin.y*scale, rect.size.width*scale, rect.size.height*scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage ], rect);
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    return newImage;
}

@end
