//
//  ThirdViewController.m
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ThirdViewController.h"

#define XCENTER 160.0

@interface ThirdViewController ()

@end

@implementation ThirdViewController

@synthesize scrollView = _scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Third", @"Third");
        self.tabBarItem.image = [UIImage imageNamed:@"Third"];
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];

    
    [super viewDidLoad];
    CGRect frame = CGRectMake(0,0,320,600);
    self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 0);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = YES;              
    [self.view addSubview:self.scrollView];   
    
    // Get the application documents directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    float yCenter = 165;
    int i = 0;
    
    while(true)
    {        
        // Create a filename string with the documents path and our filename
        NSString *fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"photo%d.png", i]];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileName])
            break;
        
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:fileName];
        if(i != 0)
            yCenter = yCenter + (image.size.height / image.size.width) * XCENTER;
        float yCenterDelta = [self createFeedItem:image withXCenter: XCENTER andYCenter: yCenter];
        yCenter += yCenterDelta + 10;
        i++;
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.scrollView.frame.size.height + yCenter - 165 * 2);
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// creates the effectTile
- (float)createFeedItem:(UIImage *)photo withXCenter: (float)xCenter andYCenter: (float)yCenter
{
    FeedItem *photoView = [FeedItem buttonWithType: UIButtonTypeCustom];
    [photoView setImage:photo forState:UIControlStateNormal];
    [photoView addTarget:self
                  action:@selector(feedItemTouched:)
        forControlEvents:UIControlEventTouchUpInside];
    
    // initialize effects quickview with the proper effect type
    float yCenterDelta = [photoView setUp];
    
    // move effects view to proper location
    photoView.center = CGPointMake(xCenter, yCenter);
    
    [self.scrollView addSubview:photoView];
    
    return yCenterDelta;
}

// tile responds to selection
- (void)feedItemTouched:(FeedItem *)item
{
    
    NSLog(@"hi");
}

@end
