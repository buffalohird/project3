//
//  EditViewController.m
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EditViewController.h"

@interface EditViewController ()

@end

@implementation EditViewController

@synthesize photo = _photo;
@synthesize effect = _effect;
@synthesize effectName = _effectName;
@synthesize navBar = _navBar;
@synthesize toolBar = _toolBar;
@synthesize photoView = _photoView;
@synthesize originalPhoto = _originalPhoto;
@synthesize effectsFilter = _effectsFilter;
@synthesize scale = _scale;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scale = 1.0;
    _translation.x = 0.0;
    _translation.y = 0.0;
    
    // disallow multiple tiles from being touched at once
    self.view.multipleTouchEnabled = YES;
    // prepare our effects applying object
    self.effectsFilter = [[EffectsFilter alloc] init];
    
    
    self.photoView = [[UIImageView alloc] init];
    
    // do the default effects to the image for whichever filter
    self.photoView.image = [self.effectsFilter defaultEffects:self.effect forImage:self.photo];
    
    float aspectRatio = self.photo.size.width / self.photo.size.height;;
    float photoViewWidth;
    float photoViewHeight;
    if(self.photo.size.width > 320)
    {
        photoViewWidth = 372.0 * aspectRatio;
        photoViewHeight = 372.0;
    }
    else
    {
        photoViewWidth = 372.0 * aspectRatio;
        photoViewHeight = 372.0;
    }
    self.photoView.frame = CGRectMake(160.0 - photoViewWidth/2.0, 230.0 - photoViewHeight/2, photoViewWidth, photoViewHeight);
    self.photoView.contentMode = UIViewContentModeScaleAspectFit;
    self.photoView.backgroundColor = [UIColor whiteColor];
    self.photoView.userInteractionEnabled = YES;
    self.photoView.multipleTouchEnabled = YES;
    

    
    [self.view insertSubview:self.photoView belowSubview:self.navBar];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePhoto:)];
    [pinchGesture setDelegate:self];
    [self.photoView addGestureRecognizer:pinchGesture];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panPhoto:)];
    [panGesture setMaximumNumberOfTouches:2];
    [panGesture setDelegate:self];
    [self.photoView addGestureRecognizer:panGesture];

    
    self.originalPhoto = self.photo;

    
    self.navBar.topItem.title = [NSString stringWithFormat:@"%@", self.effectName];
    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// scale and rotation transforms are applied relative to the layer's anchor point
// this method moves a gesture recognizer's view's anchor point between the user's fingers
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) 
    {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

- (void)panPhoto:(UIPanGestureRecognizer *)gestureRecognizer
{
    UIView *piece = [gestureRecognizer view];
    
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [gestureRecognizer translationInView:[piece superview]];
        
        [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y + translation.y)];
        [gestureRecognizer setTranslation:CGPointZero inView:[piece superview]];
    }
}

// scale the piece by the current scale
// reset the gesture recognizer's rotation to 0 after applying so the next callback is a delta from the current scale
- (void)scalePhoto:(UIPinchGestureRecognizer *)gestureRecognizer
{
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        [gestureRecognizer setScale:1];
        
    }
}

- (IBAction)done:(id)sender
{
    [self dismissModalViewControllerAnimated: YES];
}

- (IBAction)resetPhoto:(id)sender
{

    self.photo = self.originalPhoto;
    self.photoView.image = self.photo;
    //[self.view reloadInputViews];
}

- (IBAction)savePhoto:(id)sender
{
    
    // Get the application documents directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    int saveIndex = 0;
    NSString *fileName;
    while(true)
    {        
        // Create a filename string with the documents path and our filename
        fileName = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"photo%d.png", saveIndex]];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileName])
            break;
        
        saveIndex++;
    }

    // Save image to disk
    //UIImageView *newImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 44.0, 32.0, 372.0)];
    [UIImagePNGRepresentation(self.photoView.image) writeToFile:fileName atomically:YES];
}

- (IBAction)cropPhoto:(id)sender
{
    
    NSLog(@"cropPhoto"); 
    CGRect cropRect = CGRectMake(0, 0, 320, 320);
    [self commitCropPhoto:cropRect];
}

- (void)commitCropPhoto:(CGRect)cropRect
{
    
    NSLog(@"commitCropPhoto"); 
    self.photoView.image = [self.photoView.image crop: cropRect scalable:YES];
}


@end
