//
//  SecondViewController.h
//  Project3
//
//  Created by Buffalo Hird on 4/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EffectsViewController.h"
#import "GradientButton.h"

@interface SecondViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, retain) UIImageView * photoView;
@property (nonatomic, retain) UIButton *chooseButton;
@property (nonatomic, retain) UIButton *takeButton;


- (void)getPhoto:(id)sender;

@end
