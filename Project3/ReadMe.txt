
Ansel Duff
Buffalo Hird
Project3
Computer Science 164

README.txt - Instructions on constructing our iOSapplication.

What We've Done:

	* Established basic framework/skeleton/workflow
	* Written several effect methods
	* Given user capability of opening (selecting), taking, editing, and storing photos
	* Created an editing environment
    
Some Design Decisions

	* Right now, when the app is launched the user is brought to a void first tab view. This is obviously temporary as we are currently centralizing
	  a single view such that upon launch the app will give the user the option of taking a new photo, selecting one from their library, etc.
	* Right now the overlays are existing. We hope to give the user a little bit more flexibility when it comes to overlay selection/implementation/magnitude.

Known Issues/Things to Improve Upon  
	
	* As of right now, there's a bug: the effect thumbnails inadvertently rotate. Once the thumbnail is clicked, some of the effect methods turn the
	  large-scale preview back to the proper orientation but others don't. Rotation is certainly something that we will work on for the release.
	* Cropping and Retina support are still in the works as is full photo editing. All of the graphics (backgrounds, overlays, etc) have Retina counterparts which we will implement
	  for the release

Testing Notes:
1.) If you click the "Take a Photo" button on the simulator, our app crashes. The simulator can't handle camera requests. This sometimes happens when the user
	clicks the "Choose Photo" button.