//
//  ThirdViewController.h
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedItem.h"

@interface ThirdViewController : UIViewController

@property (nonatomic, retain) UIScrollView *scrollView;


- (float)createFeedItem:(UIImage *)photo withXCenter: (float)xCenter andYCenter: (float)yCenter;

@end
