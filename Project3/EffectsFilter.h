//
//  EffectsFilter.h
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaceDetector.h"

@interface EffectsFilter : NSObject

@property (nonatomic, strong) FaceDetector *facedetector;

- (UIImage*)defaultEffects:(int)effect forImage:(UIImage *)image;
- (UIImage*)updateEffects:(int)effect forImage:(UIImage *)image;
- (UIImage *)grayscale:(UIImage *)image;
- (UIImage *)sepia: (UIImage *)image;
- (UIImage *)redify: (UIImage *)image;
- (UIImage *)overlay:(UIImage *)image withOverlay:(int)overlaySetting andBlendConstant:(int)blendConstant;
- (UIImage *)hip: (UIImage *)image;
@end
