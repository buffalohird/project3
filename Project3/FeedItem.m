//
//  FeedItem.m
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FeedItem.h"

// each tile's dimensions
#define ITEMWIDTH 300
#define ITEMHEIGHT 200

@implementation FeedItem

@synthesize effectName = _effectName;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)createHighlightLayer
{
    highlightLayer = [CALayer layer];
    highlightLayer.backgroundColor = [UIColor colorWithRed: 0.10f
                                                     green: 0.10f
                                                      blue: 0.10f
                                                     alpha: 0.60].CGColor;
    highlightLayer.frame = self.layer.bounds;
    highlightLayer.hidden = YES;
    [self.layer insertSublayer:highlightLayer above:self.layer];
    
}

- (void)setHighlighted:(BOOL)highlight {
    highlightLayer.hidden = !highlight;
    [super setHighlighted:highlight];
}


- (float)setUp
{
    self->aspectRatio = self.imageView.image.size.height / self.imageView.image.size.width;
    self.frame = CGRectMake(0, 0, ITEMWIDTH, ITEMWIDTH * aspectRatio);
    self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.9];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self canBecomeFirstResponder];
    self.userInteractionEnabled = YES;
    
    
    
    //add our fancy label
    [self createLabel];
    
    
    
    return (ITEMWIDTH * self->aspectRatio) / 2;
}

// creates the label to be added to each tile's image
- (void)createLabel
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0,(ITEMWIDTH * self->aspectRatio) - 30.0,ITEMWIDTH,30.0)];
    label.text = [NSString stringWithFormat: @"%@", self.effectName];
    label.textColor = [UIColor whiteColor];
    label.textAlignment   = UITextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:14];
    label.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    label.userInteractionEnabled = NO;
    label.exclusiveTouch = NO;
    [self addSubview:label];
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
