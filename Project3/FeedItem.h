//
//  FeedItem.h
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FeedItem : UIButton {
    CALayer *highlightLayer;
    float aspectRatio;
}

@property (nonatomic, weak) NSString *effectName;

- (float)setUp;
- (void)createLabel;
- (void)createHighlightLayer;
@end
