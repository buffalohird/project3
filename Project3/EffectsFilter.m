//
//  EffectsFilter.m
//  Project3
//
//  Created by Buffalo Hird on 4/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EffectsFilter.h"

@implementation EffectsFilter

@synthesize facedetector = _facedetector;

#define radians(degrees) (degrees * M_PI/180)

// set the original effect
- (UIImage *)defaultEffects:(int)effect forImage:(UIImage *)image
{   
    return [self updateEffects: effect forImage:image];
}
// leaves space for updating (fine-tuning) of effects
- (UIImage *)updateEffects:(int)effect forImage:(UIImage *)image
{
    // decide the proper effect
    switch (effect) {
        case 1:
            return image;
            break;
        case 2:
            return [self grayscale:image];
            break;
        case 3:
            return [self sepia:image];
            break;
        case 4:
            return [self redify:image];
            break;
        case 5:
            return [self overlay:image withOverlay:0 andBlendConstant:0];
            break;
        case 6:
            return [self overlay:image withOverlay:0 andBlendConstant:1];
            break;
        case 7:
            return [self overlay:image withOverlay:1 andBlendConstant:2];
            break;
        case 8:
            return [self overlay:image withOverlay:1 andBlendConstant:0];
            break;
        case 9:
            return [self hip:image];
            break;
        default:
            break;
    }
    
    if(effect > 9 && effect < 12)
    {
        return image;
    }
    else
        return nil;
}

-(UIImage *)grayscale:(UIImage *)image
{
    // create new image rectangle with image's height
    CGRect imageRect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    // pick the grayscale colorSpace
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // create a bitmap of image and grayscale colorSpace
    CGContextRef context = CGBitmapContextCreate(nil, // data
                                                 image.size.width, // width
                                                 image.size.height, // height
                                                 8, // bitsPerComponent
                                                 0, // bytesPerRow
                                                 colorSpace, // colorSpace
                                                 kCGImageAlphaNone); // bitmapInfo
    
    // draw image into current context using grayscale colorSpace
    CGContextDrawImage(context, // contextRef
                       imageRect, // CGRect
                       [image CGImage]); //CGImageRef
    
    // Create bitmap image info with current context data
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    
    UIImage *newImage = [UIImage imageWithCGImage: imageRef];
    
    return newImage;
    
}

-(UIImage *)sepia: (UIImage *)image
{
    
    CGImageRef originalImage = [image CGImage];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef bitmapContext = CGBitmapContextCreate(NULL,
                                                       CGImageGetWidth(originalImage),
                                                       CGImageGetHeight(originalImage),
                                                       8,
                                                       CGImageGetWidth(originalImage)*4,
                                                       colorSpace,
                                                       kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(bitmapContext, CGRectMake(0, 0, CGBitmapContextGetWidth(bitmapContext), CGBitmapContextGetHeight(bitmapContext)), originalImage);
    UInt8 *data = CGBitmapContextGetData(bitmapContext);
    int numComponents = 4;
    int bytesInContext = CGBitmapContextGetHeight(bitmapContext) * CGBitmapContextGetBytesPerRow(bitmapContext);
    
    int redIn, greenIn, blueIn, redOut, greenOut, blueOut;
	
    for (int i = 0; i < bytesInContext; i += numComponents) {
		
        redIn = data[i];
        greenIn = data[i+1];
        blueIn = data[i+2];
		
        redOut = (int)(redIn * .393) + (greenIn *.769) + (blueIn * .189);
        greenOut = (int)(redIn * .349) + (greenIn *.686) + (blueIn * .168);
        blueOut = (int)(redIn * .272) + (greenIn *.534) + (blueIn * .131);		
        
        if (redOut>255) redOut = 255;
        if (blueOut>255) blueOut = 255;
        if (greenOut>255) greenOut = 255;
        
        data[i] = (redOut);
        data[i+1] = (greenOut);
        data[i+2] = (blueOut);
    }
    
    CGImageRef outImage = CGBitmapContextCreateImage(bitmapContext);
    UIImage *newImage = [UIImage imageWithCGImage:outImage];
    CGImageRelease(outImage);
    return newImage;
}

-(UIImage *)redify: (UIImage *)image
{
    
    CGImageRef originalImage = [image CGImage];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef bitmapContext = CGBitmapContextCreate(NULL,
                                                       CGImageGetWidth(originalImage),
                                                       CGImageGetHeight(originalImage),
                                                       8,
                                                       CGImageGetWidth(originalImage)*4,
                                                       colorSpace,
                                                       kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(bitmapContext, CGRectMake(0, 0, CGBitmapContextGetWidth(bitmapContext), CGBitmapContextGetHeight(bitmapContext)), originalImage);
    UInt8 *data = CGBitmapContextGetData(bitmapContext);
    int numComponents = 4;
    int bytesInContext = CGBitmapContextGetHeight(bitmapContext) * CGBitmapContextGetBytesPerRow(bitmapContext);
    
    int redIn, greenIn, blueIn, redOut, greenOut, blueOut;
	
    for (int i = 0; i < bytesInContext; i += numComponents) 
    {
		
        redIn = data[i];
        greenIn = data[i+1];
        blueIn = data[i+2];
		
        redOut = (int)(2 * redIn * .393) + (2 * greenIn *.769) + (2 * blueIn * .189);
        greenOut = (int)(redIn * .349) + (greenIn *.686) + (blueIn * .168);
        blueOut = (int)(redIn * .272) + (greenIn *.534) + (blueIn * .131);		
        
        if (redOut>255) redOut = 255;
        if (blueOut>255) blueOut = 255;
        if (greenOut>255) greenOut = 255;
        
        data[i] = (redOut);
        data[i+1] = (greenOut);
        data[i+2] = (blueOut);
    }
    
    CGImageRef outImage = CGBitmapContextCreateImage(bitmapContext);
    UIImage *newImage = [UIImage imageWithCGImage:outImage];
    CGImageRelease(outImage);
    return newImage;
}


-(UIImage *)overlay:(UIImage *)image withOverlay:(int)overlaySetting andBlendConstant:(int)blendConstant
{
    // our image to be overlayed and the rectangle wrapper to hold the image
    UIImage *overlay;
    CGRect imageRect, overlayRect;
    
    // color blocks
    if(overlaySetting == 0)
    {
        overlay = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"iPhone_overlay_blocks" ofType:@"png"]];
        imageRect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    }
    // harvard crest
    else if (overlaySetting == 1)
    {
        overlay = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"iPhone_harvard_crest" ofType:@"png"]];
        imageRect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        overlayRect = CGRectMake(0.0, 0.0, 320.0, 480.0);
    }
    
    // create a new context with size of our image in question
    CGSize size = CGSizeMake(image.size.width, image.size.height);
    UIGraphicsBeginImageContext(size);
    
    // draw the user's chosen into the rectangle
    [image drawInRect:imageRect];
    
    // decide how to blend
    switch (blendConstant)
    {
        case 0:
            [overlay drawInRect:imageRect blendMode:kCGBlendModeExclusion alpha:.85];
            break;
        case 1:
            [overlay drawInRect:imageRect blendMode:kCGBlendModeOverlay alpha:1.0];
            break;
        case 2:
            [overlay drawInRect:imageRect blendMode:kCGBlendModeMultiply alpha:1.0];
            break;            
        default:
            break;
    }
    
    // return the image to be displayed as a thumbnail
    return UIGraphicsGetImageFromCurrentImageContext();
    
}

/*
 * The hip method is serpate from the overlay method above as it is pretty different, though it serves the same functionality.
 * Here we use multiple overlays and we don't need to accept any arguments
 */

-(UIImage *)hip:(UIImage *)image
{
    // setup the overlays we'll be using
    UIImage* overlay = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"hip_overlay2" ofType:@"png"]];
    UIImage* grain = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"grain2" ofType:@"jpg"]];
    UIImage* noise = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"grain1" ofType:@"jpg"]];
    
    // establish the right image size, image rectangle
    CGSize size = CGSizeMake(image.size.width, image.size.height);
    CGRect imageRect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    // create new context, then add the overlays
    UIGraphicsBeginImageContext(size);
    [image drawInRect:imageRect blendMode:kCGBlendModeNormal alpha:.76];
    [overlay drawInRect:imageRect blendMode:kCGBlendModeColorBurn alpha:.21];
    [grain drawInRect:imageRect blendMode:kCGBlendModeMultiply alpha:.58];
    [noise drawInRect:imageRect blendMode:kCGBlendModeOverlay alpha:.6];
    
    // return the image in our current context
    return UIGraphicsGetImageFromCurrentImageContext();
}

@end
