//
//  FaceDetector.h
//  Project3
//
//  Created by Buffalo Hird on 4/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaceDetector : NSObject

-(UIImage *)setUp:(int)effect forImage:(UIImage *)image;
-(void)markFaces:(UIImageView *)facePicture;

@end
